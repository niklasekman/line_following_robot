var indexSectionsWithContent =
{
  0: "abcdelopqrsuw",
  1: "l",
  2: "acdlu",
  3: "bcdlopqsuw",
  4: "r",
  5: "elo"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Enumerations",
  5: "Macros"
};

