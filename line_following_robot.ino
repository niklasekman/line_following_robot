/** 
 *  @file   line_following_robot.ino 
 *  @brief  A line following and obstacle avoiding rover 
 *  @author Niklas Ekman 
 *  @date   2021-11-16 
 ***********************************************/

#include <PID_v1.h>
#include <QTRSensors.h>
#include "DFMobile.h"
#include "URM37.h"
#include "TimerOneServo.h"
#include <movingAvg.h>          // https://github.com/JChristensen/movingAvg
#include "BMS.h"


//#define DEBUG 1           ///< Master switch for serial debug messages
//#define DEBUG_WHEELS 1    ///< Enable debug messages relating to driving
//#define DEBUG_QTR 1       ///< Enable debug messages relating to line following
//#define DEBUG_ULTRA 1     ///< Enable debug messages relating to ultrasound readings
//#define DEBUG_STATES 1    ///< Enable debug messages relating to state machine
//#define DEBUG_OBST_PID 1  ///< Enable debug messages relating to obstacle avoidance PID
//#define DEBUG_BATTERY 1   ///< Enable debug messages relating to battery management

#define EN_WHEELS 1         ///< Enable output to motor drivers
#define EN_QTR 1            ///< Enable reading of line sensor data

#define LINE_KP 0.125 * 0.6     ///< Set P gain for line following
#define LINE_KI 0.0125          ///< Set I gain for line following
#define LINE_KD LINE_KP * 0.005 ///< Set D gain for line following

#define OBSTACLE_KP 7           ///< Set P gain for obstacle avoidance
#define OBSTACLE_KI 1           ///< Set I gain for obstacle avoidance
#define OBSTACLE_KD 1           ///< Set D gain for obstacle avoidance

/** @brief An instance of the servo handler class */
TimerOneServo servo = TimerOneServo(1099, 4899, 0, 180); // OCR1B_min, OCR1B_max, rangeMin, rangeMax

/** @brief A pointer to an instance of the ultrasound handling class */
URM37* ultra;

/** @brief An instance of the moving average filter. Average over the three latest readings */
movingAvg ultranoise(3);

/** @brief An instance of the  */
DFMobile wheels = DFMobile(4,5,7,6); // left_en, left_pwm, right_en, right_pwm

// Initialize variables only if reading of line sensor data has been enabled
#ifdef EN_QTR
/** @brief For holding an instance of the line sensor library class */
QTRSensors qtr;
/** @brief Use all 8 IR sensors of the device */
const uint8_t SensorCount = 8;
/** @brief Array for holding a single reading from each IR sensor */
uint16_t sensorValues[SensorCount];
#endif

/** @brief The previous line position reported by the sensor */
uint16_t prevPosition = 0;

/** @brief The distance reported by the ultrasound sensor  */
unsigned int distance = 0;

/** @brief Prevoius recorded milliseconds since the start of the program  */
unsigned long previousMillis = 0;
/** @brief Interval for running the line following loop in ms  */
const long drivingInterval = 10;
/** @brief Interval for distance measurement and obstacle avoidance loop  */
const long pingInterval = 50;

/** @brief Variables for the line following PID  */
double lineSetpoint, lineInput, lineOutput;
/** @brief Instance of the PID object for line following  */
PID linePID(&lineInput, &lineOutput, &lineSetpoint,LINE_KP,LINE_KI,LINE_KD, DIRECT);

/** @brief Variables for the obstacle avoidance PID  */
double obstacleSetpoint, obstacleInput, obstacleOutput;
/** @brief Instance of the PID object fow line following */
PID obstaclePID(&obstacleInput, &obstacleOutput, &obstacleSetpoint,OBSTACLE_KP,OBSTACLE_KI,OBSTACLE_KD, DIRECT);

/** @brief Enumeration used by the state machine  */
enum RobotState {
  DRIVING,
  FOUND_OBSTACLE,
  FOLLOWING_OBSTACLE,
  STOP
};

/** @brief For keeping track of the state  */
RobotState state = DRIVING;

/** @brief Speed to be driving at when following line, 0-255  */
int cruiseSpeed = 192;
/** @brief Speed to be driving at when avoiding obstacle, 0-255  */
int obstacleSpeed = 192;

/** @brief Pins to read battery cell voltages from  */
int pins[] = {4,5};
/** @brief Instance of the battery management object  */
BMS bms = BMS(pins, 2);
/** @brief For keeping track of the cell voltages  */
double cellVoltages[2];
/** @brief Minimum cell voltage */
double battMinVoltage = 3.2;
/** @brief Maximum difference in voltage between cells  */
double battMaxCellDiffVoltage = 0.5;

void setup() {
  // Start serial communications if debug master switch is on
  #ifdef DEBUG
  Serial.begin(9600);
  #endif

  // Instantiate an object for reading the ultrasound sensor and make the
  // pointer point to it
  ultra = new URM37(3,2,PWM_INT); // triggerPin, sensorPin, mode
  
  // Use pointer notation to call the init function.
  // Pass a lambda function which calls the interrupt service routine
  // member function of the class. This is required for the library
  // to be able to handle interrupts behind the scenes instead
  // of having to implement an ISR in the main program.
  ultra->init([]{ultra->echoIsr();});

  // Initialize the servo library
  servo.init();
  // Set the initial direction to 90° (forward)
  servo.setDirection(90);

  // If the motor drivers are enabled, make sure the wheels are not turning
  #ifdef EN_WHEELS
  wheels.Speed(0,0);
  #endif

  // If the line sensor is enabled
  #ifdef EN_QTR
  // Set the type to be digital
  qtr.setTypeRC();
  // Pin configuration
  qtr.setSensorPins((const uint8_t[]){8, 9, 11, 12, 13, 14, 15, 16}, SensorCount);
  qtr.setEmitterPin(3);

  // If debug messages for the line sensor are enabled
  #ifdef DEBUG_QTR
  Serial.println("Calibrating line sensor");
  #endif

  // Calibrate the line sensor
  for (uint16_t i = 0; i < 200; i++)
  {
    qtr.calibrate();
  }
  #endif

  // Turn the line following PID on (automatic: on, manual: off)
  linePID.SetMode(AUTOMATIC);
  // PID ouputs a steering value between -75° and 75°
  linePID.SetOutputLimits(-75, 75);
  // Compute at same frequency as the line following loop
  linePID.SetSampleTime(drivingInterval); 
  // Keep in the middle of the line
  lineSetpoint = 0;

  // Turn the obstacle avoiding PID on
  obstaclePID.SetMode(AUTOMATIC);
  // PID ouputs a steering value between -75° and 75°
  obstaclePID.SetOutputLimits(-75, 75);
  // Compute at same frequency as the ultrasound reading loop
  obstaclePID.SetSampleTime(pingInterval); 
  // Keep a set distance from the obstacle
  obstacleSetpoint = 35; // cm

  // Start the moving average filter
  ultranoise.begin();
}

/** @brief Check battery state and change to the stopped state if any of the cells go under 
the minumum cell voltage or if the voltage difference becomes to large */
void checkBattery() {
  bms.getVoltages(cellVoltages);
  #ifdef DEBUG_BATTERY
  Serial.print(cellVoltages[0]);
  Serial.print('\t');
  Serial.println(cellVoltages[1]);
  #endif
  if(cellVoltages[0] < battMinVoltage || cellVoltages[1] < battMinVoltage || abs(cellVoltages[0] - cellVoltages[1]) > battMaxCellDiffVoltage) {
    state = STOP;
  }
}

/** @brief Main line following loop. 
    Read in a line position from the line sensor.
    Compute the PID algorithm and translate the calculated
    steering angle and desired rover speed to wheel speeds. Output wheel speeds to the motor drivers.

    @param speed the desired rover speed, 0-255
*/
void drive(int speed) {
    #ifdef EN_QTR
    uint16_t position = qtr.readLineBlack(sensorValues);  // Read distance from ultrasound sensor
    #ifdef DEBUG_QTR
    Serial.println(position);
    #endif
    #else
    uint16_t position = 3500;                             // If the line sensor isn't enabled, 
    #endif                                                // set the position variable to a value which corresponds
                                                          // to the line being in the middle of the sensor. 
    lineInput = (double)position - 3500;                  // Subtract 3500 from the position value to make the line being in the
                                                          // Middle of the sensor correspond to a value of 0 for the PID algorithm
    linePID.Compute();                                    // Compute PID algorithm

    double theta = (lineOutput + 135) * 0.01745329251;    // Rotate the output from the PID by 135° and convert from degrees to radians
    double x = -speed * cos(theta);                       // Translate polar coordinates to cartesian
    double y = -speed * sin(theta);
    #ifdef DEBUG_WHEELS                                   
    Serial.print(-x);
    Serial.print('\t');
    Serial.println(y);
    #endif
    #ifdef EN_WHEELS                                      // Feed the cartesian coordinates to the motor controllers
    wheels.Speed(-y,x); // left motors, right motors
    #endif
}

/** @brief Check reading from ultrasound sensor while following line to see if there's an obstacle ind the way.
          If the reading is less than 20 cm transition to the obstacle found state.
*/
void checkObstacleDriving() {
  int dist = ultra->getDistance_cm();
  
  #ifdef DEBUG_ULTRA
  Serial.println(dist);
  #endif
  
  if(dist < 20) {
    state = FOUND_OBSTACLE;
  }
}

/** @brief Main obstacle following loop. 
The difference from the line following loop is that the distance from the ultrasound sesor is used as the input to the PID.
Before this loop is entered the rover has positioned itself 90° perpendicular to the line it has followed. The ultrasound sensor 
is also rotated 70° off to the left by the servo beforehand. This is so that a constant error is introduced to the PID calculation
which results in the rover driving forward while still following the obstacle at a distance. If the sensor were to point directly
at the obstacle, i.e. 90° to the left, then the rover would try to make the distance meet the setpoint by simply turning on the spot
without any forward momentum.

If the difference in values from the line sensor from the previous iteration is over a certain threshold it is decided that the rover 
now has returned to the line. The state then changes to line following.

@param speed the desired rover speed, 0-255
*/
void avoidObstacle(int speed) {
  int dist = ultra->getDistance_cm();                     // Read distance from ultrasound sensor
  int distFlt = ultranoise.reading(dist);                 // Apply moving average filtering to sensor value

  obstacleInput = (double)distFlt;                        // Cast filtered value to double for PID algorithm
  obstaclePID.Compute();                                  // Compute PID algorithm

  double theta = (obstacleOutput + 135) * 0.01745329251;  // Rotate the output from the PID by 135° and convert from degrees to radians
  double x = -speed * cos(theta);                         // Translate polar coordinates to cartesian
  double y = -speed * sin(theta);

  #ifdef DEBUG_OBST_PID
  Serial.print(obstacleInput);
  Serial.print('\t');
  Serial.println(obstacleOutput);
  #endif
  
  #ifdef DEBUG_WHEELS
  Serial.print(-x);
  Serial.print('\t');
  Serial.println(y);
  #endif
  #ifdef EN_WHEELS                                         // Feed the cartesian coordinates to the motor controllers
  wheels.Speed(-y,x); // left motors, right motors
  #endif


  #ifdef EN_QTR
  uint16_t position = qtr.readLineBlack(sensorValues);      // Take a reading from the line sensor
  #endif
  if(abs(prevPosition - position) > 3000) {                 // If the reading differs enough from the last iteration
    servo.setDirection(90);
    wheels.Speed(-255,-255);
    delay(50);
    wheels.Speed(0,0);                                      // stop
    delay(500);
    state = DRIVING;                                        // and begin followin the line again
  }
  prevPosition = position;
}

/** @brief Main program loop. 
Houses a big switch ... case structure which essentially is the entire state machine of the rover.

In the driving state the battery health is checked, the line following algorithm is calculated and obstacles are being detected.

In the found obstacle state the rover is stopped and turned perpendicular to the line, the servo is turned to face the obstacle
and the state is transitioned to the obstacle following state.

In the obstacle following state the logic is refactored out to the avoidObstacle function.

The stopped state only does just that; stops the rover. There is no recourse from this state.
*/
void loop() {
  unsigned long currentMillis = millis();
  
  if (currentMillis - previousMillis >= drivingInterval) {  // run loop at specified interval
    switch(state) {
      case DRIVING:
        checkBattery();
        drive(cruiseSpeed);
        #ifdef DEBUG_STATES
        Serial.println("Driving!");
        #endif
        if (currentMillis - previousMillis >= pingInterval) {   // Use different interval for reading ultrasound sensor
          checkObstacleDriving();
        }
        break;
      case FOUND_OBSTACLE:
        #ifdef DEBUG_STATES
        Serial.println("Found obstacle!");
        #endif
        wheels.Speed(-255,-255);    // Stop
        delay(50);
        wheels.Speed(-255,255);     // Turn to the right
        delay(400);
        wheels.Speed(0,0);
        servo.setDirection(160);    // Turn ultrasound sensor to the left
        delay(500);
        prevPosition = qtr.readLineBlack(sensorValues);   // Record line sesor value for use inside the following obsacle state
        state = FOLLOWING_OBSTACLE;
        break;
      case FOLLOWING_OBSTACLE:
        #ifdef DEBUG_STATES
        Serial.println("Following obstacle!");
        #endif
        if (currentMillis - previousMillis >= pingInterval) {
          avoidObstacle(obstacleSpeed);
        }
        break;
      case STOP:
        wheels.Speed(0,0);
        #ifdef DEBUG_STATES
        Serial.println("Stopped");
        #endif
        break;
      default:
        break;
    }
  }
}
